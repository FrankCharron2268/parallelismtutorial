﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Parallelism.WaitingForTasks
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var cts = new CancellationTokenSource();
            var token = cts.Token;

            var t = new Task(() =>
            {
                Console.WriteLine("I take 5 seconds");

                for (int i = 0; i < 5; i++)
                {
                    token.ThrowIfCancellationRequested();
                    Thread.Sleep(1000);
                }
                Console.WriteLine("Task 1 done");
            }, token);
            t.Start();

            Task t2 = Task.Factory.StartNew(() =>
            {
                Thread.Sleep(3000);
                Console.WriteLine("Task 2 Done");
            }, token);

            if (Console.ReadKey(true).Key == ConsoleKey.Spacebar)       //That's how you do an action on a specific key
            {
                cts.Cancel();
            }

            //t.Wait(token); // Waits the referred task to finish
            Task.WaitAny(new[] { t, t2 }, 4000, token); //This one will wait for any tasks
            //Task.WaitAll(new[] { t, t2 }, token);//Waits for Tasks to finish can take an array of tasks.

            Console.Write($"\nTask t  status is {t.Status}");
            Console.Write($"\nTask t2 status is {t2.Status}");

            Console.WriteLine("Main program done");
            Console.ReadKey();
        }
    }
}