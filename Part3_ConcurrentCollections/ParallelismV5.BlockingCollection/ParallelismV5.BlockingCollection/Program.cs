﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ParallelismV5.BlockingCollection
{                     //Producer consumer pattern
    internal static class Program
    {
        private static BlockingCollection<int> messages =
           new BlockingCollection<int>(new ConcurrentBag<int>(), 10);//Creates a a concurrent bag with a wrapper around it that will block incoming messages when limit is reached. Preventing it from overflowing.

        private static CancellationTokenSource cts = new CancellationTokenSource();

        private static Random random = new Random();

        private static void ProduceAndConsume()
        {
            var producer = Task.Factory.StartNew(RunProducer);
            var consumer = Task.Factory.StartNew(RunConsumer);

            try
            {
                Task.WaitAll(new[] { producer, consumer }, cts.Token);
            }
            catch (AggregateException ae)
            {
                ae.Handle(e => true);
            }
        }

        private static void Main(string[] args)
        {
            Task.Factory.StartNew(ProduceAndConsume, cts.Token);
            Console.ReadKey();
            cts.Cancel();
        }

        private static void RunConsumer()
        {     //consumes items in the collection
            foreach (var item in messages.GetConsumingEnumerable()) //basically this will get you an element as soon as there is one.
            {
                cts.Token.ThrowIfCancellationRequested();
                Console.WriteLine($"-{item}\t");
                Thread.Sleep(random.Next(1000));
            }
        }

        private static void RunProducer()
        {
            while (true) // produces integers and adds them to the collection
            {
                cts.Token.ThrowIfCancellationRequested();
                int i = random.Next(100);
                messages.Add(i);
                Console.WriteLine($"+{i}\t");
                Thread.Sleep(random.Next(1000));
            }
        }
    }
}