﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParallelismV4.ConcurrentBag
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            //Stack  LIFO  : Last in First out structure.
            //Queue FiFO : First in First out structure.
            //Bag NO : No ordering structure.

            var bag = new ConcurrentBag<int>();
            var tasks = new List<Task>();

            for (int i = 0; i < 10; i++)
            {
                var i1 = i;
                tasks.Add(Task.Factory.StartNew(() =>
                {
                    bag.Add(i1);
                    Console.WriteLine($"Task {Task.CurrentId} has added {i1}");
                    int result;
                    if (bag.TryPeek(out result))
                    {
                        Console.WriteLine($"Task {Task.CurrentId} has peeked the value {result}");
                    }
                }));
            }

            Task.WaitAll(tasks.ToArray());

            int last;
            if (bag.TryTake(out last))
            {
                Console.Write($"I got {last}"); //as you can see its kind of random which element will be taken out as there are no order in this structure
            }
        }
    }
}