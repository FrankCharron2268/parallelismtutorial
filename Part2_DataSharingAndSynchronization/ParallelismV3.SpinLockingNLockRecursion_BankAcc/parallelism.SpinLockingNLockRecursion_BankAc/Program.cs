﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace parallelism.SpinLockingNLockRecursion_BankAc
{
    public class BankAccount
    {
        public int Balance { get; private set; }
        public object padlock = new object();

        public void Deposit(int amount)
        {
            Balance += amount;
        }

        public void Withdraw(int amount)
        {
            Balance -= amount;
        }
    }

    internal static class Program
    {
        private static void Main(string[] args)
        {
            var tasks = new List<Task>();
            var ba = new BankAccount();

            SpinLock sl = new SpinLock();  // With will spin the thread without yielding until it is able to execute.

            #region This sectio's code should have a result balance of Zero but because the operations are not atomic we get a random bizare result because they are interrupted.

            //When the operations become atomic with the lock then it gives a result of zero
            for (int i = 0; i < 10; i++)
            {
                tasks.Add(Task.Factory.StartNew(() =>
                {
                    for (int j = 0; j < 1000; j++)
                    {
                        var lockTaken = false;
                        try
                        {
                            sl.Enter(ref lockTaken);//Try to take lock if available bool turns true and operation▼ will execute. If timeout it'll fail
                            ba.Deposit(100);
                        }
                        finally
                        {
                            if (lockTaken) sl.Exit();//if we did manage to take the lock then we close it
                        }
                    }
                }));
                tasks.Add(Task.Factory.StartNew(() =>
                {
                    for (int j = 0; j < 1000; j++)
                    {
                        var lockTaken = false;
                        try
                        {
                            sl.Enter(ref lockTaken);            //Try to take lock if available bool turns true and operation▼ will execute. If timeout itll fail
                            ba.Withdraw(100);
                        }
                        finally
                        {
                            if (lockTaken) sl.Exit();     //if we did manage to take the lock then we close it
                        }
                    }
                }));
            }
            Task.WaitAll(tasks.ToArray());    //Waits for all tasks to complete

            Console.WriteLine($"Final Balance is {ba.Balance }.");

            #endregion This sectio's code should have a result balance of Zero but because the operations are not atomic we get a random bizare result because they are interrupted.
        }
    }
}