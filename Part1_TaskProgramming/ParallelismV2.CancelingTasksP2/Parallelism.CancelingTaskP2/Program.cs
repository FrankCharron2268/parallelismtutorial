﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection.Emit;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Parallelism.CancelingTaskP2
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var planned = new CancellationTokenSource();
            var preventative = new CancellationTokenSource();
            var emergency = new CancellationTokenSource();

            var paranoid = CancellationTokenSource.CreateLinkedTokenSource(
                planned.Token, preventative.Token, emergency.Token);         //Links the cancellation tokens together so when we use one paranoid token instead of the specific token

            Task.Factory.StartNew(() =>
            {
                int i = 0;

                while (true)
                {
                    paranoid.Token.ThrowIfCancellationRequested();
                    Console.WriteLine($"\t{i++}");
                    Thread.Sleep(1000);
                }
            }, paranoid.Token);
        returnLabel:

            var result = Console.ReadKey();

            switch (result.KeyChar)
            {
                case 'q':
                    planned.Cancel();
                    Console.WriteLine($"\tPlanned Cancellation was used");
                    break;

                case 'w':
                    preventative.Cancel();
                    Console.WriteLine($"\tPreventative Cancellation was used");
                    break;

                case 'e':
                    emergency.Cancel();
                    Console.WriteLine($"\tEmergency Cancellation was used");
                    break;

                default:
                    goto returnLabel;
            }

            Console.WriteLine("Main program done");
            Console.ReadKey();
        }
    }
}