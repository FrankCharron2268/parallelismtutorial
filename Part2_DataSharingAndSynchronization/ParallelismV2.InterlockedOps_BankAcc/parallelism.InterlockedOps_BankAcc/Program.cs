﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace parallelism.CriticalSections_BankAcc
{
    public class BankAccount
    {
        public int balance;

        public int Balance
        {
            get { return balance; }
            private set { balance = value; }
        }

        public object padlock = new object();

        public void Deposit(int amount)
        {
            //Balance += amount;
            //'+=' is not atomic because it amounts to those operations
            //op1: temp <-get_Balance() + amount
            //op2: set_Balance(temp)

            Interlocked.Add(ref balance, amount); //Interlocked provides atomic operations
            Interlocked.MemoryBarrier();
        }

        public void Withdraw(int amount)
        {
            Interlocked.Add(ref balance, -amount); //Interlocked don't have a subtract method so we just add -amount instead.
        }
    }

    internal static class Program
    {
        private static void Main(string[] args)
        {
            var tasks = new List<Task>();
            var ba = new BankAccount();

            for (int i = 0; i < 10; i++)
            {
                tasks.Add(Task.Factory.StartNew(() =>
                {
                    for (int j = 0; j < 1000; j++)
                    {
                        ba.Deposit(100);
                    }
                }));
                tasks.Add(Task.Factory.StartNew(() =>
                {
                    for (int j = 0; j < 1000; j++)
                    {
                        ba.Withdraw(100);
                    }
                }));
            }

            Task.WaitAll(tasks.ToArray());

            Console.WriteLine($"Final Balance is {ba.balance}.");
        }
    }
}