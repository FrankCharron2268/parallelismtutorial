﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using System.Threading.Tasks;

namespace Parallelism.Waiting
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            //var t = new Task(() =>
            //{
            //    Thread.Sleep();//Pause the task for x long
            //    SpinWait.SpinUntil(); // Pauses the task but it doesn't lose its position in the execution scheme.
            //});

            var cts = new CancellationTokenSource();
            var token = cts.Token;

            var t = new Task(() =>
            {
                Console.WriteLine("Press any key to disarm, You have 5 seconds");
                //Basically create a handle that waits until it is triggered to cancel a task
                bool cancelled = token.WaitHandle.WaitOne(5000); //by linking it to the bool we have feedback and we know the cancel worked
                Console.WriteLine(cancelled ? "Bomb Disarmed! " : "Boom");  //by passing it here we will get a message according to result.
            }, token);
            t.Start();         //Starts our task

            Console.ReadKey();               //waits for input
            cts.Cancel();

            Console.WriteLine("Main program done");
            Console.ReadKey();
        }
    }
}