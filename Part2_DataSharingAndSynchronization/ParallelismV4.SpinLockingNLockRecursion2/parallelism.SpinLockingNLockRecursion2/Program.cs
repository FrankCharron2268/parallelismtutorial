﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace parallelism.SpinLockingNLockRecursion2
{
    internal static class Program
    {
        //A recursive lock :  Meaning =  You take a lock and then you call the function again and try to take that lock without►
        //it being released will cause you to deadlock whereas if you do specify a true argument here▼
        //then you're going to catch exceptions (LockRecursionException)
        private static SpinLock sl = new SpinLock(true); //True : enables thread owner tracking allowing to know which thread took the lock

        public static void LockRecursion(int x)
        {
            //This is just to show that when a lock is taken the next operation cannot proceed and will fail to take a lock.
            bool lockTaken = false;
            try
            {
                sl.Enter(ref lockTaken); // takes a lock if it succeeds turns lockTaken true
            }
            catch (LockRecursionException e)
            {
                Console.WriteLine("Exception: " + e); //Throwing this error
            }
            finally
            {
                if (lockTaken)
                {
                    Console.WriteLine($"Took a lock, x = {x}"); // 'x' is unimportant, it is just to show that operations changed.
                    LockRecursion(x - 1);  //Now that lock is taken we re-invoke the task and it will fail, ▲27

                    sl.Exit();
                }
                else
                {
                    Console.WriteLine($"Failed to take a lock, x={x}");
                }
            }
        }

        public static void Main(string[] args)
        {
            LockRecursion(5); //could be any number doesnt matter.
        }
    }
}