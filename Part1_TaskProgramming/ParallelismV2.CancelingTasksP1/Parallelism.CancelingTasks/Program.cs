﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Parallelism.CancelingTasks
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var cts = new CancellationTokenSource();
            var token = cts.Token; //I don't understand how it cancels on any key input.

            token.Register(() => { Console.WriteLine("Cancelation has been requested."); });

            var t = new Task(() =>         //Builds a task with the following parameters (Action, CancellationToken)
            {
                int i = 0;
                while (true)
                {
                    //Way 1:This is the recommended way of doing it. As it leaves a record of the fact that somebody cancelled the operation.
                    token.ThrowIfCancellationRequested(); //Way 1:Canonical way of cancelling an operation/task.

                    //if (token.IsCancellationRequested) //Way 2:looks if cancellation token was requested.
                    //{
                    //    throw new OperationCanceledException();//Another way of doing the same thing. But throws an exception and the result is different than above.
                    //}

                    //if (token.IsCancellationRequested) //Looks if cancellation token was requested.
                    //    break;      //Way 3:Will also work. But this way is silent
                    //else
                    Console.WriteLine($"{i++}");
                }
            }, token);
            t.Start();

            Task.Factory.StartNew(() =>
            {    //Way 4 : Creates a handle that waits until it is released to cancel the task.
                token.WaitHandle.WaitOne();
                Console.WriteLine("Wait handle released, cancellation requested.");
            });

            Console.ReadKey();
            cts.Cancel();    //The task is canceled when this is called.

            Console.WriteLine("Main program done.");
            Console.ReadKey();
        }
    }
}