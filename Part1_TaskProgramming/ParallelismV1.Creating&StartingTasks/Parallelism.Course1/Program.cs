﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parallelism.Course1
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            ///One way to start a task.
            //Task.Factory.StartNew(() => Write('.'));

            ///Different way to start a task.
            //var t = new Task(() => Write('?'));
            //t.Start();

            //Write('-');

            ///Other ways to provide arguments to tasks
            //Task.Factory.StartNew(Write, 123);

            //Task t = new Task(Write, "hello");
            //t.Start();

            ///Ways to start a task that returns a value.
            string text1 = "testing", text2 = "this";

            var task1 = new Task<int>(TextLenght, text1);
            task1.Start();

            Task<int> task2 = Task.Factory.StartNew<int>(TextLenght, text2);
            //Output the results. which is basically blocking the task until it gets it.
            Console.WriteLine($"Lenght of '{text1}' is {task1.Result}");
            Console.WriteLine($"Lenght of '{text2}' is {task2.Result}");

            Console.WriteLine("Main program done.");
            Console.ReadKey();
        }

        public static void Write(char c)
        {
            int i = 1000;
            while (i-- > 0)
            {
                Console.Write(c);
            }
        }

        public static void Write(object o)
        {
            int i = 1000;
            while (i-- > 0)
            {
                Console.Write(o);
            }
        }

        public static int TextLenght(object o)  //return lenght of text
        {
            Console.WriteLine($"\nTask with id {Task.CurrentId} processing object {o}");
            return o.ToString().Length;
        }
    }
}