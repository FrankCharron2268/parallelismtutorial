﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace parallelism.CriticalSections_BankAcc
{
    public class BankAccount
    {
        public int Balance { get; private set; }
        public object padlock = new object();

        public void Deposit(int amount)
        {
            //Balance += amount;
            //'+=' is not atomic because it amounts to those operations
            //op1: temp <-get_Balance() + amount
            //op2: set_Balance(temp)
            lock (padlock)//This locks the operation so it does not get interrupted...▼
            {
                Balance += amount;
            }
        }

        public void Withdraw(int amount)
        {
            //Balance -= amount;

            //When another thread tries to do something like this operation while ▲ operation is ongoing then it has to wait.
            //Making the operations Atomic.
            lock (padlock)
            {
                Balance -= amount;
            }
        }
    }

    internal static class Program
    {
        private static void Main(string[] args)
        {
            var tasks = new List<Task>();
            var ba = new BankAccount();

            #region This sectio's code should have a result balance of Zero but because the operations are not atomic we get a random bizare result because they are interrupted.

            //When the operations become atomic with the lock then it gives a result of zero
            for (int i = 0; i < 10; i++)
            {
                tasks.Add(Task.Factory.StartNew(() =>
                {
                    for (int j = 0; j < 1000; j++)
                    {
                        ba.Deposit(100);
                    }
                }));
                tasks.Add(Task.Factory.StartNew(() =>
                {
                    for (int j = 0; j < 1000; j++)
                    {
                        ba.Withdraw(100);
                    }
                }));
            }
            Task.WaitAll(tasks.ToArray());

            Console.WriteLine($"Final Balance is {ba.Balance }.");

            #endregion This sectio's code should have a result balance of Zero but because the operations are not atomic we get a random bizare result because they are interrupted.
        }
    }
}