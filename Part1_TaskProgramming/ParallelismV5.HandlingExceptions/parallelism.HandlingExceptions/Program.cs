﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace parallelism.HandlingExceptions
{
    internal static class Program
    {
        public static void Main()
        {
            try
            {
                Test();
            }
            catch (AggregateException ae)
            {   //Do anything with caught exceptions e.g verify if it is ok to continue.
                foreach (var e in ae.InnerExceptions)
                {
                    Console.WriteLine($"\nHandled elsewhere: {ae.GetType()}"); //This will catch all exceptions that haven't been dealt with
                }
            }
            Console.WriteLine("Main Program done");
            Console.ReadKey();
        }

        private static void Test()
        {
            var t = Task.Factory.StartNew(() => throw new InvalidOperationException("Can't do this!") { Source = "t" });

            var t2 = Task.Factory.StartNew(() => throw new AccessViolationException("Can't access this!") { Source = "t2" });

            try
            {
                Task.WaitAll(t, t2);
            }
            catch (AggregateException ae)
            {
                //foreach (var e in ae.InnerExceptions) Console.WriteLine($"Exception {e.GetType()} from {e.Source}");//Catches every exception throw
                ae.Handle(e =>
                {
                    if (e is InvalidOperationException)       //Create a Handle that will be invoke when only this exception is thrown.
                    {
                        Console.Write("Invalid operation!");
                        return true;//by returning true we have dealt with the exception.
                    }
                    else return false;
                });
            }
        }
    }
}