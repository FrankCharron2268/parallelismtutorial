﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParallelismV2.ConcurrentQueue
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            var q = new ConcurrentQueue<int>();
            q.Enqueue(1);
            q.Enqueue(2);

            int result;

            if (q.TryDequeue(out result)) //will try and remove first queued element
            {
                Console.WriteLine($"Removed element {result}");
            }

            if (q.TryPeek(out result))  //peek at the first queued element
            {
                Console.WriteLine($"Front element is {result}");
            }
        }
    }
}