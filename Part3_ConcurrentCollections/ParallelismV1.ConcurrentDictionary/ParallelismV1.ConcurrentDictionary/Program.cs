﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParallelismV1.ConcurrentDictionary
{
    internal static class Program
    {
        private static ConcurrentDictionary<string, string> capitals = new ConcurrentDictionary<string, string>();

        public static void AddParis()
        {
            bool success = capitals.TryAdd("France", "Paris"); //since tryadd returns a bool lets save it
            string who = Task.CurrentId.HasValue ? ("Task " + Task.CurrentId) : "Main Thread"; //to find out which thread succeded
            Console.WriteLine($"{who}{(success ? "added" : "did not add")} the element.");
        }

        private static void Main(string[] args)
        {     //adding to ConcurrentDictionary
            Task.Factory.StartNew(AddParis).Wait();    //will succeed
            AddParis();  //will fail

            //Lets modify an Element in the ConcurrentDictionary.
            //capitals["Russia"] = "Leningrad";       //in this example if we comment the line adding Leningrad to Russia
            capitals.AddOrUpdate("Russia", "Moscow", (k, old) => old + " --> Moscow");  //Shows the transition between new and old. Giving you both values if there was a previous value.
            Console.WriteLine($"The capital of Russia is {capitals["Russia"]}");  //

            //other example
            capitals["Sweden"] = "Uppsala";//comment this to change result and view what happens
            var capOfSweden = capitals.GetOrAdd("Sweden", "Stockholm");  //if there's an entry already it gets the value if there's none it sets it
            Console.WriteLine($"The capital of Sweden is {capOfSweden}");

            //Lets remove an element from the ConcurrentDictionary
            const string toRemove = "Russia";
            string removed;
            var didRemove = capitals.TryRemove(toRemove, out removed);
            {
                if (didRemove)
                {
                    Console.WriteLine($"We just removed {removed}");
                }
                else
                {
                    Console.WriteLine($"Failed to remove the capital of {toRemove}");
                }
                //if you want to enumerate a dictionary it will be less costly to doing it like this▼ rather than capitals.count.
                foreach (var kv in capitals)
                {
                    Console.WriteLine($" - {kv.Value} is the capital of {kv.Key} ");
                }
            }
        }
    }
}